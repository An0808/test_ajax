<html>
<head><title>Basic Ajax Demo</title>
    <script type="text/javascript" src="https://code.jquery.com/jquery-latest.pack.js"></script>
    <script>
        $(document).ready(function () {
            jQuery("#btn").click(function(){
                var data_test = ['a','b','c'];
                $.ajax({
                    url: 'vietnam.php',
                    type: 'POST',
                    data: 'string='+data_test,
                    success: function (data) {
                        setTimeout(function(){
                            var arr = JSON.parse(data);
                            console.log(arr.a);
                            $('#demo-ajax').html(data.a);
                        },30);
                    },
                    error: function (e) {
                        console.log(e.message);
                    }
                });
            });
        });
    </script>
</head>
<body>
<div id="demo-ajax"></div>
<button id="btn">View more</button>
</body>
</html>